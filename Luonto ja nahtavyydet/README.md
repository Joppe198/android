# Luonto ja nähtävyydet

Työvoimakoulutuksen aikana aloitin suunittelemaan Luonto ja nähtävyydet sovellusta.
Sovelluksessa on pelkästään ne kohteet joissa olen käynyt.

## Sovelluksen käyttö tarkoitus

Sovelluksessa käyttäjä voi suunistaa valitulle reitti pisteelle, joita ovat: Luontopolut ja niiden parkki ja nuotio paikat, näkötornit, museot sekä nähtävyydet ja muistomerkit.

## Sovelluksen tarvittavat laite tiedot

- Käyttää Android 8 tai uudempaa käyttöjärjestelmää.
- Internet yhteys
- GPS yhteys
- Sovellus toimii Suomeksi ja Englanniksi
- Google Maps

## Sovelluksen Versiointi Kotlin

Versio 1.0

- Kartta tyylit
- Reitti pisteet
- Zoomi toiminto painikkeina ja sormin
- Oman olinpaikan näyttäminen kartalta painike
  
## Sovvelluksen JSON paikka tiedot

Versio 1.0

1. Luontopolut
   1. Hitonhauta
   2. Karhunahas
   3. Koiviston
2. Näkötornit
3. Museot
   1. Raatteen portti
4. Nähtävyydet ja muistomerkit
   1. Venäläinen muistomerkki
   2. Talvisodan muistomerkki
   3. Ukrainalainen muistomerkki
   4. Raatteen tien entisöidyt puolustus linja ja korsut
   5. Kajaanin vanhan linnan rauniot

## Kuvat

![Aloitus kuva](Kuvia/aloitus_kuva.jpg)
![Menu](Kuvia/toinen_kuva.jpg)
![Kartta tyylit](Kuvia/kolmas_kuva.jpg)
![Reitti pisteet](Kuvia/neljas_kuva.jpg)
