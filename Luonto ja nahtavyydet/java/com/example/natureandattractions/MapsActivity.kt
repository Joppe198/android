package com.example.natureandattractions

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.example.natureandattractions.databinding.ActivityMapsBinding
import com.google.android.gms.maps.model.*
import org.json.JSONArray


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding
    private val LOCATION_PERMISSION = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera
        val finland = LatLng(61.920441, 26.229463)
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(finland, 4F))

        // Show My location button
        enableMyLocation()
        mMap.uiSettings.isZoomControlsEnabled = true


    }

    // Created menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.map_options, menu)
        return true
    }

    // Menu Options
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Change the map type based on the user's selection.
        return when (item.itemId) {
            R.id.normal_map -> {
                mMap.mapType = GoogleMap.MAP_TYPE_NORMAL
                true
            }
            R.id.satellite_map -> {
                mMap.mapType = GoogleMap.MAP_TYPE_SATELLITE
                true
            }
            R.id.terrain_map -> {
                mMap.mapType = GoogleMap.MAP_TYPE_TERRAIN
                true
            }
            R.id.parking -> {
                mMap.clear()
                loadDataParking()
                true
            }
            R.id.campfire -> {
                mMap.clear()
                loadDataCampfire()
                true
            }
            R.id.nature_trail -> {
                mMap.clear()
                loadDataNature_Trail()
                true
            }
            R.id.museum -> {
                mMap.clear()
                loadDataMuseum()
                true
            }
            R.id.overlooking_tower -> {
                mMap.clear()
                loadDataOverlooking_Tower()
                true
            }
            R.id.attractions_and_monument -> {
                mMap.clear()
                loadDataAttractions_And_Monuments()
                true
            }
            else  -> super.onOptionsItemSelected(item)
        }
    }


    // Enable my location buttons
    private fun enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            this.mMap.isMyLocationEnabled = true
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION)
        }
    }

    //Permission request location
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // Check if location permission are granted and if so enable the location data layer.

        when (requestCode) {
            LOCATION_PERMISSION -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                enableMyLocation()
            }
        }
    }


   /* private fun loadDataAll() {
        val queue = Volley.newRequestQueue(this)
        val url = "Oma osoite"
        var place: JSONArray
        val placeTypes: Map<String, Float> = mapOf(
            "parking" to BitmapDescriptorFactory.HUE_BLUE,
            "nature trail" to BitmapDescriptorFactory.HUE_GREEN,
            "campfire" to BitmapDescriptorFactory.HUE_RED,
            "museum" to BitmapDescriptorFactory.HUE_AZURE,
            "overlooking tower" to BitmapDescriptorFactory.HUE_YELLOW,
            "attractions and monument" to BitmapDescriptorFactory.HUE_ORANGE
        )

        // create JSON request object
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.GET, url, null,
            { response ->
                // JSON loaded successfully
                place = response.getJSONArray("showplace")
                // loop through all objects
                for (i in 0 until place.length())
                {
                    val place = place.getJSONObject(i)
                    val lat = place["lat"].toString().toDouble()
                    val lng = place["lng"].toString().toDouble()
                    val cord = LatLng(lat, lng)
                    val type = place["type"].toString()
                    val title = place["place"].toString()
                    val web = place["web"].toString()
                    val travel = place["travel"].toString()
                    val text = place["text"].toString()

                    if(placeTypes.containsKey(type))
                    {
                        val m = mMap.addMarker(
                            MarkerOptions()
                                .position(cord)
                                .title(title)
                                .icon(BitmapDescriptorFactory
                                    .defaultMarker(placeTypes.getOrDefault(type, BitmapDescriptorFactory.HUE_ROSE)
                                    )
                                )
                        )
                        // pass data to marker via Tag
                        val list = listOf(web, travel, text)
                        m.setTag(list)
                    }
                }

            },
            { error ->
                // Error loading JSON
            }
        )
        // Add the request to the RequestQueue
        queue.add(jsonObjectRequest)
        // Add custom info window adapter here
        mMap.setInfoWindowAdapter(AllInfoWindowAdapter())
    }
    */

    @RequiresApi(Build.VERSION_CODES.N)
    private fun loadDataParking() {
        val queue = Volley.newRequestQueue(this)
        val url = "Oma osoite"
        var place: JSONArray
        val placeTypes: Map<String, Float> = mapOf(
            "parking" to BitmapDescriptorFactory.HUE_BLUE,
        )

        // create JSON request object
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.GET, url, null,
            { response ->
                // JSON loaded successfully
                place = response.getJSONArray("showplace")
                // loop through all objects
                for (i in 0 until place.length())
                {
                    val place = place.getJSONObject(i)
                    val lat = place["lat"].toString().toDouble()
                    val lng = place["lng"].toString().toDouble()
                    val cord = LatLng(lat, lng)
                    val type = place["type"].toString()
                    val title = place["place"].toString()
                    val text = place["text"].toString()

                    if(placeTypes.containsKey(type))
                    {
                        val m = mMap.addMarker(
                            MarkerOptions()
                                .position(cord)
                                .title(title)
                                .icon(BitmapDescriptorFactory
                                    .defaultMarker(placeTypes.getOrDefault(type, BitmapDescriptorFactory.HUE_ROSE)
                                    )
                                )
                        )
                        // pass data to marker via Tag
                        val list = listOf(text)
                        m.setTag(list)
                    }
                }

            },
            { error ->
                // Error loading JSON
            }
        )
        // Add the request to the RequestQueue
        queue.add(jsonObjectRequest)
        // Add custom info window adapter here
        mMap.setInfoWindowAdapter(ParkingInfoWindowAdapter())
    }

    private fun loadDataNature_Trail() {
        val queue = Volley.newRequestQueue(this)
        val url = "Oma osoite"
        var place: JSONArray
        val placeTypes: Map<String, Float> = mapOf(
            "nature trail" to BitmapDescriptorFactory.HUE_GREEN,
        )

        // create JSON request object
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.GET, url, null,
            { response ->
                // JSON loaded successfully
                place = response.getJSONArray("showplace")
                // loop through all objects
                for (i in 0 until place.length())
                {
                    val place = place.getJSONObject(i)
                    val lat = place["lat"].toString().toDouble()
                    val lng = place["lng"].toString().toDouble()
                    val cord = LatLng(lat, lng)
                    val type = place["type"].toString()
                    val title = place["place"].toString()
                    val web = place["web"].toString()
                    val travel = place["travel"].toString()
                    val text = place["text"].toString()

                    if(placeTypes.containsKey(type))
                    {
                        val m = mMap.addMarker(
                            MarkerOptions()
                                .position(cord)
                                .title(title)
                                .icon(BitmapDescriptorFactory
                                    .defaultMarker(placeTypes.getOrDefault(type, BitmapDescriptorFactory.HUE_ROSE)
                                    )
                                )
                        )
                        // pass data to marker via Tag
                        val list = listOf(web, travel, text)
                        m.setTag(list)
                    }
                }

            },
            { error ->
                // Error loading JSON
            }
        )
        // Add the request to the RequestQueue
        queue.add(jsonObjectRequest)
        // Add custom info window adapter here
        mMap.setInfoWindowAdapter(Nature_travellerInfoWindowAdapter())
    }

    private fun loadDataCampfire() {
        val queue = Volley.newRequestQueue(this)
        val url = "Oma osoite"
        var place: JSONArray
        val placeTypes: Map<String, Float> = mapOf(
            "campfire" to BitmapDescriptorFactory.HUE_RED,
        )

        // create JSON request object
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.GET, url, null,
            { response ->
                // JSON loaded successfully
                place = response.getJSONArray("showplace")
                // loop through all objects
                for (i in 0 until place.length())
                {
                    val place = place.getJSONObject(i)
                    val lat = place["lat"].toString().toDouble()
                    val lng = place["lng"].toString().toDouble()
                    val cord = LatLng(lat, lng)
                    val type = place["type"].toString()
                    val title = place["place"].toString()
                    val web = place["web"].toString()
                    val travel = place["travel"].toString()
                    val text = place["text"].toString()

                    if(placeTypes.containsKey(type))
                    {
                        val m = mMap.addMarker(
                            MarkerOptions()
                                .position(cord)
                                .title(title)
                                .icon(BitmapDescriptorFactory
                                    .defaultMarker(placeTypes.getOrDefault(type, BitmapDescriptorFactory.HUE_ROSE)
                                    )
                                )
                        )
                        // pass data to marker via Tag
                        val list = listOf(text)
                        m.setTag(list)
                    }
                }

            },
            { error ->
                // Error loading JSON
            }
        )
        // Add the request to the RequestQueue
        queue.add(jsonObjectRequest)
        //  Add custom info window adapter here
        mMap.setInfoWindowAdapter(CampfireInfoWindowAdapter())
    }

    private fun loadDataMuseum() {
        val queue = Volley.newRequestQueue(this)
        val url = "Oma osoite"
        var place: JSONArray
        val placeTypes: Map<String, Float> = mapOf(
            "museum" to BitmapDescriptorFactory.HUE_AZURE,
        )

        // create JSON request object
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.GET, url, null,
            { response ->
                // JSON loaded successfully
                place = response.getJSONArray("showplace")
                // loop through all objects
                for (i in 0 until place.length())
                {
                    val place = place.getJSONObject(i)
                    val lat = place["lat"].toString().toDouble()
                    val lng = place["lng"].toString().toDouble()
                    val cord = LatLng(lat, lng)
                    val type = place["type"].toString()
                    val title = place["place"].toString()
                    val web = place["web"].toString()
                    val travel = place["travel"].toString()
                    val text = place["text"].toString()

                    if(placeTypes.containsKey(type))
                    {
                        val m = mMap.addMarker(
                            MarkerOptions()
                                .position(cord)
                                .title(title)
                                .icon(BitmapDescriptorFactory
                                    .defaultMarker(placeTypes.getOrDefault(type, BitmapDescriptorFactory.HUE_ROSE)
                                    )
                                )
                        )
                        // pass data to marker via Tag
                        val list = listOf(web, travel, text)
                        m.setTag(list)
                    }
                }

            },
            { error ->
                // Error loading JSON
            }
        )
        // Add the request to the RequestQueue
        queue.add(jsonObjectRequest)
        //  Add custom info window adapter here
        mMap.setInfoWindowAdapter(MuseumInfoWindowAdapter())
    }

    private fun loadDataOverlooking_Tower() {
        val queue = Volley.newRequestQueue(this)
        val url = "Oma osoite"
        var place: JSONArray
        val placeTypes: Map<String, Float> = mapOf(
            "overlooking tower" to BitmapDescriptorFactory.HUE_YELLOW,
        )

        // create JSON request object
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.GET, url, null,
            { response ->
                // JSON loaded successfully
                place = response.getJSONArray("showplace")
                // loop through all objects
                for (i in 0 until place.length())
                {
                    val place = place.getJSONObject(i)
                    val lat = place["lat"].toString().toDouble()
                    val lng = place["lng"].toString().toDouble()
                    val cord = LatLng(lat, lng)
                    val type = place["type"].toString()
                    val title = place["place"].toString()
                    val web = place["web"].toString()
                    val travel = place["travel"].toString()
                    val text = place["text"].toString()

                    if(placeTypes.containsKey(type))
                    {
                        val m = mMap.addMarker(
                            MarkerOptions()
                                .position(cord)
                                .title(title)
                                .icon(BitmapDescriptorFactory
                                    .defaultMarker(placeTypes.getOrDefault(type, BitmapDescriptorFactory.HUE_ROSE)
                                    )
                                )
                        )
                        // pass data to marker via Tag
                        val list = listOf(web, travel, text)
                        m.setTag(list)
                    }
                }

            },
            { error ->
                // Error loading JSON
            }
        )
        // Add the request to the RequestQueue
        queue.add(jsonObjectRequest)
        //  Add custom info window adapter here
        mMap.setInfoWindowAdapter(Overlooking_towerInfoWindowAdapter())
    }

    private fun loadDataAttractions_And_Monuments() {
        val queue = Volley.newRequestQueue(this)
        val url = "Oma osoite"
        var place: JSONArray
        val placeTypes: Map<String, Float> = mapOf(
            "attractions and monument" to BitmapDescriptorFactory.HUE_ORANGE
        )

        // create JSON request object
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.GET, url, null,
            { response ->
                // JSON loaded successfully
                place = response.getJSONArray("showplace")
                // loop through all objects
                for (i in 0 until place.length())
                {
                    val place = place.getJSONObject(i)
                    val lat = place["lat"].toString().toDouble()
                    val lng = place["lng"].toString().toDouble()
                    val cord = LatLng(lat, lng)
                    val type = place["type"].toString()
                    val title = place["place"].toString()
                    val web = place["web"].toString()
                    val travel = place["travel"].toString()
                    val text = place["text"].toString()

                    if(placeTypes.containsKey(type))
                    {
                        val m = mMap.addMarker(
                            MarkerOptions()
                                .position(cord)
                                .title(title)
                                .icon(BitmapDescriptorFactory
                                    .defaultMarker(placeTypes.getOrDefault(type, BitmapDescriptorFactory.HUE_ROSE)
                                    )
                                )
                        )
                        // pass data to marker via Tag
                        val list = listOf(web, text)
                        m.setTag(list)
                    }
                }

            },
            { error ->
                // Error loading JSON
            }
        )
        // Add the request to the RequestQueue
        queue.add(jsonObjectRequest)
        //  Add custom info window adapter here
        mMap.setInfoWindowAdapter(AttractionsAndMonumentsInfoWindowAdapter())
    }



    /* internal inner class AllInfoWindowAdapter : GoogleMap.InfoWindowAdapter {

        @SuppressLint("InflateParams")
        private val contents: View = layoutInflater.inflate(R.layout.info_windows_all, null)

        override fun getInfoWindow(marker: Marker?): View? {
            return null
        }

        override fun getInfoContents(marker: Marker?): View {
            // UI elements
            val titleTextView = contents.findViewById<TextView>(R.id.titletextView)
            val websiteTextView = contents.findViewById<TextView>(R.id.websitetextView)
            val travel_to_TextView = contents.findViewById<TextView>(R.id.travel_to_textView)
            val infotitleTextView = contents.findViewById<TextView>(R.id.infotitletextView)
            // title
            titleTextView.text = marker?.title.toString()
            // get data from Tag list
            if (marker?.tag is List<*>) {
                val list : List<String> = marker.tag as List<String>
                websiteTextView.text = list[0]
                travel_to_TextView.text = list[1]
                infotitleTextView.text = list[2]
            }
            return contents
        }
    } */

    internal inner class ParkingInfoWindowAdapter : GoogleMap.InfoWindowAdapter {

        @SuppressLint("InflateParams")
        private val contents: View = layoutInflater.inflate(R.layout.info_windows_parking, null)

        override fun getInfoWindow(marker: Marker?): View? {
            return null
        }

        override fun getInfoContents(marker: Marker?): View {
            // UI elements
            val titleTextView = contents.findViewById<TextView>(R.id.titletextView)
            val infotitleTextView = contents.findViewById<TextView>(R.id.infotitletextView)
            // title
            titleTextView.text = marker?.title.toString()
            // get data from Tag list
            if (marker?.tag is List<*>) {
                val list : List<String> = marker.tag as List<String>
                infotitleTextView.text = list[0]
            }
            return contents
        }
    }

    internal inner class CampfireInfoWindowAdapter : GoogleMap.InfoWindowAdapter {

        @SuppressLint("InflateParams")
        private val contents: View = layoutInflater.inflate(R.layout.info_windows_campfire, null)

        override fun getInfoWindow(marker: Marker?): View? {
            return null
        }

        override fun getInfoContents(marker: Marker?): View {
            // UI elements
            val titleTextView = contents.findViewById<TextView>(R.id.titletextView)
            val infoTextView = contents.findViewById<TextView>(R.id.infotitletextView)
            // title
            titleTextView.text = marker?.title.toString()
            // get data from Tag list
            if (marker?.tag is List<*>) {
                val list : List<String> = marker.tag as List<String>
                infoTextView.text = list[0]
            }
            return contents
        }
    }

    internal inner class Nature_travellerInfoWindowAdapter : GoogleMap.InfoWindowAdapter {

        @SuppressLint("InflateParams")
        private val contents: View = layoutInflater.inflate(R.layout.info_windows_naturel_trail, null)

        override fun getInfoWindow(marker: Marker?): View? {
            return null
        }

        override fun getInfoContents(marker: Marker?): View {
            // UI elements
            val titleTextView = contents.findViewById<TextView>(R.id.titletextView)
            val websiteTextView = contents.findViewById<TextView>(R.id.websitetextView)
            val traveltoTextView = contents.findViewById<TextView>(R.id.traveltotextView)
            val infoTextView = contents.findViewById<TextView>(R.id.infotitletextView)
            // title
            titleTextView.text = marker?.title.toString()
            // get data from Tag list
            if (marker?.tag is List<*>) {
                val list : List<String> = marker.tag as List<String>
                websiteTextView.text = list[0]
                traveltoTextView.text = list[1]
                infoTextView.text = list[2]
            }
            return contents
        }
    }

    internal inner class MuseumInfoWindowAdapter : GoogleMap.InfoWindowAdapter {

        @SuppressLint("InflateParams")
        private val contents: View = layoutInflater.inflate(R.layout.info_windows_museum, null)

        override fun getInfoWindow(marker: Marker?): View? {
            return null
        }

        override fun getInfoContents(marker: Marker?): View {
            // UI elements
            val titleTextView = contents.findViewById<TextView>(R.id.titletextView)
            val websiteTextView = contents.findViewById<TextView>(R.id.websitetextView)
            val infoTextView = contents.findViewById<TextView>(R.id.infotitletextView)
            // title
            titleTextView.text = marker?.title.toString()
            // get data from Tag list
            if (marker?.tag is List<*>) {
                val list : List<String> = marker.tag as List<String>
                websiteTextView.text = list[0]
                infoTextView.text = list[1]
            }
            return contents
        }
    }

    internal inner class Overlooking_towerInfoWindowAdapter : GoogleMap.InfoWindowAdapter {

        @SuppressLint("InflateParams")
        private val contents: View = layoutInflater.inflate(R.layout.info_windows_overlooking_tower, null)

        override fun getInfoWindow(marker: Marker?): View? {
            return null
        }

        override fun getInfoContents(marker: Marker?): View {
            // UI elements
            val titleTextView = contents.findViewById<TextView>(R.id.titletextView)
            val websiteTextView = contents.findViewById<TextView>(R.id.websitetextView)
            val infoTextView = contents.findViewById<TextView>(R.id.infotitletextView)
            // title
            titleTextView.text = marker?.title.toString()
            // get data from Tag list
            if (marker?.tag is List<*>) {
                val list : List<String> = marker.tag as List<String>
                websiteTextView.text = list[0]
                infoTextView.text = list[1]
            }
            return contents
        }
    }

    internal inner class AttractionsAndMonumentsInfoWindowAdapter : GoogleMap.InfoWindowAdapter {

        @SuppressLint("InflateParams")
        private val contents: View = layoutInflater.inflate(R.layout.info_windows_attractions_and_monument, null)

        override fun getInfoWindow(marker: Marker?): View? {
            return null
        }

        override fun getInfoContents(marker: Marker?): View {
            // UI elements
            val titleTextView = contents.findViewById<TextView>(R.id.titletextView)
            val websiteTextView = contents.findViewById<TextView>(R.id.websitetextView)
            val infoTextView = contents.findViewById<TextView>(R.id.infotitletextView)
            // title
            titleTextView.text = marker?.title.toString()
            // get data from Tag list
            if (marker?.tag is List<*>) {
                val list : List<String> = marker.tag as List<String>
                websiteTextView.text = list[0]
                infoTextView.text = list[1]
            }
            return contents
        }
    }

}
