package com.example.magiclifecounter

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Get button widget
        val standardBtn = findViewById<Button>(R.id.standardBtn)
        val commanderBtn = findViewById<Button>(R.id.commanderBtn)
        val legalBtn = findViewById<Button>(R.id.legalBtn)

        //Button go to standard activity
        standardBtn.setOnClickListener {
            startActivity(Intent(this, StandardActivity::class.java))
        }

        //Button go to commander activity
        commanderBtn.setOnClickListener {
            startActivity(Intent(this, CommanderActivity::class.java))
        }

        //Button go to legal commander activity
        legalBtn.setOnClickListener {
            startActivity(Intent(this, LegalCommanderActivity::class.java))     }
    }
}