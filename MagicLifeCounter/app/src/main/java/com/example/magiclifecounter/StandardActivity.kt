package com.example.magiclifecounter


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class StandardActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_standard)

        // Get button and textView widgets
        val lifeCounter = findViewById<TextView>(R.id.lifecountertextView)
        val plus = findViewById<Button>(R.id.plusbutton)
        val minus = findViewById<Button>(R.id.minusbutton)
        val count = findViewById<TextView>(R.id.LifeeditText)
        val life2Counter = findViewById<TextView>(R.id.life2countertextView)
        val plus2 = findViewById<Button>(R.id.plus2button)
        val minus2 = findViewById<Button>(R.id.minus2button)
        val count2 = findViewById<TextView>(R.id.life2editText)




        // Plus Player 1 lifeCounter
        plus.setOnClickListener {

            val val1 = lifeCounter.text.toString().toInt()
            val val2 = count.text.toString().toInt()

            val sum = val1 + val2

            lifeCounter.setText(sum.toString())

        }


        // Minus Player 1 life lifeCounter
        minus.setOnClickListener {

            val val1 = lifeCounter.text.toString().toInt()
            val val2 = count.text.toString().toInt()

            val reduce = val1 - val2

            lifeCounter.setText(reduce.toString())
        }

        // Plus Player 2 lifeCounter
        plus2.setOnClickListener {

            val val1 = life2Counter.text.toString().toInt()
            val val2 = count2.text.toString().toInt()

            val sum2 = val1 + val2

            life2Counter.setText(sum2.toString())

        }


        // Minus Player 2 life lifeCounter
        minus2.setOnClickListener {

            val val1 = life2Counter.text.toString().toInt()
            val val2 = count2.text.toString().toInt()

            val reduce2 = val1 - val2

            life2Counter.setText(reduce2.toString())
        }

    }


    
}